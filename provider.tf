provider "azurerm" {
  features {}
  subscription_id = "0a1fca4b-fe80-493c-945b-bf30cd34a94f"
}

# State Backend

terraform {
 backend "azurerm" {
  resource_group_name = "tfstate"
  storage_account_name = "tfstatepauline"
  container_name = "tfstate"
  key = "antho.terraform.tfstate"
 }
}